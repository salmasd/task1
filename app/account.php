<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account extends Model

{
	 public function account_tpye()
    {
        return $this->hasMany('App\account_tpye');
    }
   
	protected $table = 'account';
	public $timestamps = true;


	protected $fillable = [
		'name', 'type_id','data_open','settlement', 
	];
}
