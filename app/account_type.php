<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account_type extends Model
{
   public function account()
    {
        return $this->belongsTo('App\Models\account');
    }


	protected $table = 'account_type';
	public $timestamps = true;


	protected $fillable = [
		'name',  
	];
}
