<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});
  

  Route::get('/plog', function () {
    return view('plog1');
});


  Route::get('/index', function () {
    return view('plog1');
})->name('index');


    Route::get('/index2', function () {
    return view('index-2');
})->name('index2');

        Route::get('/index3', function () {
    return view('index-3');
})->name('index3');
   

   Route::get('/index4', function () {
    return view('index-4');
}) ->name('index4');
   
   
   Route::get('/index5', function () {
    return view('index-5');
})->name('index5');


   Route::get('/index6', function () {
    return view('index-6');
})->name('index6');
   


   Route::get('/index7', function () {
    return view('index7');
})->name('index7');
   

 Route::get('/about', function () {
    return view('about-us');
})->name('about');
   
  Route::get('/contact', function () {
    return view('contact-us');
})->name('contact');
   
Route::get('/recover', function () {
    return view('recover-account');
})->name('recover'); 
  

  Route::get('/reviews', function () {
    return view('reviews');
})->name('reviews'); 


Route::get('/selcte', function () {
    return view('selcte');
})->name('selcte'); 


Route::get('/signin', function () {
    return view('signin');
})->name('signin'); 



Route::get('/signup', function () {
    return view('signup');
})->name('signup'); 



Route::get('/error', function () {
    return view('error-404');
})->name('error'); 


Route::get('/faqs', function () {
    return view('faqs');
})->name('faqs'); 

Route::get('/single', function () {
    return view('single-post');
})->name('single'); 

Route::get('/coming', function () {
    return view('coming-soon');
})->name('coming'); 

Route::get('/plog2', function () {
    return view('plog-2');
})->name('plog2'); 




Route::get('insert','AcountController@index');
Route::post('create','AcountController@create');

Route::get('selcte','AcountController@store');
Route::get('edit/{id}','AcountController@edit');