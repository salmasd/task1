<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <script src="{{ asset('js/bootstrap.min.js')}}"></script>
</head>
<body>

<form action = "/slecte" method = "get">

@csrf

<table class="table">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">type</th>
      <th scope="col">settlement</th> 
      <th scope="col">edit</th>
      
    </tr>
  </thead>
  @foreach($account as $account)
  <tbody>
    <tr > 
    <tr>
      <th scope="row">{{$account->id}}</th>
      <td>{{$account->name}}</td>
      <td>{{$account->type_id}}</td>
      <td>{{$account->settlement}}</td>
      <td><a href="{{url('edit/'.$account->id)}}" type="button" class="btn btn-danger">update</a></td>
    </tr>
    </tbody>       
 @endforeach
 </table>

</form>
</body>
</html>
