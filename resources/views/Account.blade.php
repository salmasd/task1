<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <script src="{{ asset('js/bootstrap.min.js')}}"></script>
</head>
<body>
@if (session('status'))
   <div class="alert alert-success" role="alert">
       <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('status') }}
   </div>
@elseif(session('failed'))
<div class="alert alert-danger" role="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  {{ session('failed') }}
</div>
@endif

    
<form action = "/create" method = "post">

    @csrf
      <div class="form-row">
        <div class="form-group col-md-6">
            <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name:</label>
            <input type="text" class="form-control "  id="name" name="name" >
      </div>

        <div class="form-group col-md-6">
             <label for="data_open">Date open:</label>
            <input type="Date" class="form-control" id="data_open" name="data_open"  >
       </div> 

          <div class="form-group col-md-6">
             <label for="type">type of account</label>
               <select class="form-control" name="type_id">
                     @foreach($types as $type)
                     <option value="{{$type->id}}">{{$type->name}}</option>
                      @endforeach
               </select>
          </div>

          <div class="form-group col-md-6">

                 <label for="settlement">settlement</label>
                 <input type="text" class="form-control"id="settlement" name="settlement" >
          </div>
      </div>
    

                   <input type="submit" value="Submit">
  </form> 
  </body>
</html>