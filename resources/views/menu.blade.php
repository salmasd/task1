  
  <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">الرئيسية</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ url('index') }}">الصفحة الرئيسية - 01</a>
                                    <a class="dropdown-item" href="{{ url('index') }}">الصفحة الرئيسية - 02</a>
                                    <a class="dropdown-item" href="{{ url('index2') }}">الصفحة الرئيسية - 03</a>
                                    <a class="dropdown-item" href="{{ url('index3') }}">الصفحة الرئيسية - 03</a>
                                    <a class="dropdown-item" href="{{ url('index4') }}">الصفحة الرئيسية - 04</a>
                                    <a class="dropdown-item" href="{{ url('index5') }}">الصفحة الرئيسية - 05</a>
                                    <a class="dropdown-item" href="{{ url('index6') }}">لصفحة الرئيسية - 06</a>
                                    <a class="dropdown-item" href="{{ url('index7') }}">فحة الرئيسية - 07</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="#" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">الصفحات</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                    <a class="dropdown-item" href="{{ url('about') }}">من نحن</a>
                                    <a class="dropdown-item" href="{{ url('contact')}}">اتصل بنا</a>
                                    <a class="dropdown-item" href="{{ url('faqs') }}">االأسئلة الشائعة</a>
                                    <a class="dropdown-item" href="{{ url('reviews') }}">اراء العملاء</a>
                                    <a class="dropdown-item" href="{{ url('signin') }}">تسجيل دخول</a>
                                    <a class="dropdown-item" href="{{ url('signup') }}">تسجيل مستخدم</a>
                                    <a class="dropdown-item" href="{{ url('recover') }}">استعادة كلمة المرور</a>
                                    <a class="dropdown-item" href="{{ url('coming') }}">لظهور قريبا</a>
                                    <a class="dropdown-item" href="{{ url('error') }}">صفحة الخطأ 404</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="#" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">المقالات</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3">
                                    <a class="dropdown-item" href="{{ url('plog') }}">المقالات - العرض الأول</a>
                                    <a class="dropdown-item" href="{{ url('plog2') }}">مقالات - العرض الثاني</a>
                                    <a class="dropdown-item" href="{{ url('single') }}">صفحة المقال</a>
                                </div>
                            </li>
                            <li class="nav-item"><a href="{{ url('about') }}">المميزات</a></li>
                            <li class="nav-item"><a href="{{ url('about') }}">كيف يعمل</a></li>
                            <li class="nav-item"><a href="{{ url('about') }}">لصور</a></li>
                            <li class="nav-item"><a href="{{ url('about') }}">الأسعار</a></li>
                            <li class="nav-item"><a href="{{ url('reviews') }}">االأراء</a></li>
                            <li class="nav-item"><a href="{{ url('faqs') }}">لأسئلة المتكررة</a></li>
                            <li class="nav-item"><a href="{{ url('contact')}}">اتصل بنا</a></li>
                        </ul>
                    </div>